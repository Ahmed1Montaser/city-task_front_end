import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-bootstrap-spinner';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'city-task';
  constructor(private spinner: NgxSpinnerService) { }
  ngOnInit() {

    this.spinner.show();

    setTimeout(() => {

      this.spinner.hide();
    }, 2000);
  }
}
