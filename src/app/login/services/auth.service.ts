import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { MessageBox } from 'src/app/model/message-box';
import { User } from 'src/app/model/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: User = new User("", "");
  jwt: string = "";
  loggedin: boolean = false;

  constructor(private http: HttpClient, private rout: Router) { }

  authinticate(userName: string, password: string): void {

    this.user.userName = userName;
    this.user.password = password;
    this.http.post("/api/authenticate", this.user).subscribe(
      (response: any) => {
        this.jwt = response.jwttocken;
        this.loggedin = true;
        localStorage.setItem("jwt", this.jwt);
        if (this.loggedin)
          this.rout.navigate(['city']);
      },
      error => {
        MessageBox.displaySuccess("You are un authorized , please try again later", true);
        this.loggedin = false;
      }
    );
  }

  getJwt(): string {
    if (this.jwt === "") {
      this.jwt = localStorage.getItem("jwt")!;
    }
    return this.jwt;
  }

  logout(): void {
    localStorage.setItem("jwt", "");
    this.rout.navigate(['']);
  }




}