import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageBox } from 'src/app/model/message-box';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login-component',
  templateUrl: './login-component.component.html',
  styleUrls: ['./login-component.component.css']
})
export class LoginComponentComponent implements OnInit {
  formGroup: FormGroup;
  constructor(private rout: Router, private authService: AuthService) {
    this.formGroup = new FormGroup({
      "userName": new FormControl(),
      "password": new FormControl()

    });

  }

  ngOnInit(): void {
  }


  authinticat(): void {

    let name = this.formGroup.value["userName"];
    let password = this.formGroup.value["password"];
    this.authService.authinticate(name, password);

  }


}
