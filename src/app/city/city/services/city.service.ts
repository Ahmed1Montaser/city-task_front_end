import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { City } from 'src/app/model/city';

@Injectable({
  providedIn: 'root'
})
export class CityService {
  array: string[] = [];
  subscribedCity: City[] = [];
  behave: BehaviorSubject<City[]> = new BehaviorSubject<City[]>(this.subscribedCity);
  observable: Observable<City[]> = this.behave.asObservable();


  constructor(private httpClient: HttpClient) { }

  getCityNamesBasedOn(name: String): string[] {
    let arr: string[] = [];
    let cities: City[];
    this.httpClient.get<City[]>("api/city-name/" + name).subscribe(
      (response: City[]) => {
        cities = response;
        this.array = [];
        for (let i = 0; i < cities.length; ++i) {
          this.array.push(cities[i].name);
        }
      }
    );
    return this.array;
  }

  getPageNumber(page: number): Observable<City[]> {
    return this.httpClient.get<City[]>("api/page/" + page);
  }
  getCityByName(name: String): Observable<City[]> {
    return this.httpClient.get<City[]>("api/city-name/" + name);
  }
  updateCity(name: string, photo: string, id: string): Observable<any> {
    let city: City = { name: name, photo: photo, id: id };
    return this.httpClient.put("api/edit/city", city);
  }
}
