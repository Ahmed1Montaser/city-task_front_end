import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-bootstrap-spinner';
import { AuthService } from 'src/app/login/services/auth.service';
import { City } from 'src/app/model/city';
import { MessageBox } from 'src/app/model/message-box';
import { CityService } from './services/city.service';





@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.css']
})
export class CityComponent implements OnInit {
  page: number = 0;
  formGroup: FormGroup;
  cities: City[] = [];
  disableNext: boolean = false;
  disablePrev: boolean = false;

  constructor(private spinner: NgxSpinnerService, private router: Router, private authService: AuthService, private cityService: CityService) {
    this.formGroup = new FormGroup({
      cityName: new FormControl()
    });
  }

  ngOnInit(): void {
    this.loadSpinner();
    this.retrievePageNumber(this.page);
    this.cityService.observable.subscribe((response: City[]) => {
      this.cities = response;
    });

  }

  loadSpinner(): void {
    this.spinner.show();
    setTimeout(() => {
      this.spinner.hide();
    }, 2000);
  }

  logout(): void {
    this.authService.logout();
  }

  retrievePageNumber(pageNo: number): void {
    this.cityService.getPageNumber(pageNo).subscribe(response => {
      this.cities = response;

    })
  }

  previous(): void {
    if ((this.page - 1) != -1) {
      this.retrievePageNumber(--this.page);
      this.disablePrev = false;
    }
    else {
      this.disablePrev = true;
    }
  }

  next(): void {
    if ((this.page + 1) != 1001) {
      this.retrievePageNumber(++this.page);
      this.disablePrev = false;
    }
    else {
      this.disableNext = true;
    }
  }

  toggelModal(city: City): void {
    $('#exampleModalCenter').modal('toggle');
    $('#cityName').val(city.name);
    $('#cityPhoto').val(city.photo);
    document.getElementById('Modalcity')!.innerHTML = "Edit City with Id " + city.id;
    document.getElementById('idValue')!.innerHTML = city.id;

  }

  saveChanges(): void {
    let cityName: any = $('#cityName').val();
    let cityURL: any = $('#cityPhoto').val();
    let id: string = document.getElementById('idValue')!.innerHTML;
    let message: string = "City with id " + id + " is updated !";
    this.cityService.updateCity(cityName, cityURL, id).subscribe((response: City) => {
      let index = this.cities.findIndex(function (city: City) {
        return city.id === id;
      });
      this.cities.splice(index, 1);
      this.cities.splice(index, 0, response);
      MessageBox.displaySuccess(message, false);
    },
      error => {
        MessageBox.displaySuccess("you are un authorized to access this service !", true);

      });

    this.closeModal();

  }

  closeModal(): void {

    $('#exampleModalCenter').modal('hide');
  }

}

