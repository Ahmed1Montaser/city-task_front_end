import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CityComponent } from './city/city/city.component';
import { LoginComponentComponent } from './login/login-component/login-component.component';
import { GuardService } from './login/services/guard.service';

const routes: Routes = [
  { path: "", component: LoginComponentComponent },
  { path: "city", component: CityComponent, canActivate: [GuardService] }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
