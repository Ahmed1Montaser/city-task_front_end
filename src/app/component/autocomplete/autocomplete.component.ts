import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { map, Observable, startWith } from 'rxjs';
import { CityService } from 'src/app/city/city/services/city.service';
import { City } from 'src/app/model/city';

$(document).ready(function () {
  $('#texter').blur(function () {
    if ($('#texter').val() == "")
      $('#mlabel').show();
  });

});



@Component({
  selector: 'app-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.css']
})
export class AutocompleteComponent implements OnInit {
  myControl = new FormControl();
  options: string[] = ['Cairo', 'Toronto', 'Kuwait City'];
  filteredOptions: Observable<string[]> = new Observable();
  constructor(private service: CityService) {

  }

  ngOnInit() {
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => (typeof value === 'string' ? value : value.name)),
      map(name => (name ? this._filter(name) : this.options.slice())),
    );
  }

  displayFn(user: any): string {
    return '';
  }

  private _filter(name: string): string[] {
    let arr: string[] = this.service.getCityNamesBasedOn(name);
    if (name == null || "" == name)
      this.options.push("Hello");
    else {
      this.options = [];
      for (let i = 0; i < arr.length; ++i) {
        this.options.push(arr[i]);
      }
    }
    return this.options;
  }

  bulrMe() {
    if ($('#texter').val() == "" || $('#texter').val() == null)
      $('#mlabel').show();
  }
  clicked(name: string): void {
    $('#texter').val(name);
    $('#mlabel').hide();
    this.service.getCityByName(name).subscribe((response: City[]) => {
      this.service.behave.next(response);
    });
  }
}
