import * as bootstrap from "bootstrap";
export class MessageBox {

    static displaySuccess(message: string, error: boolean): void {
        if (!error) {
            document.getElementById('toastmessage')!.innerHTML = "<img src='assets/images/right.jpg' class='rounded me-2' alt='...'>" + message;
            document.getElementById('action')!.innerHTML = "Done";
        } else {
            document.getElementById('toastmessage')!.innerHTML = "<img src='assets/images/wrong.jpg' class='rounded me-2' alt='...'>" + message;
            document.getElementById('action')!.innerHTML = "Error";
        }
        var toastElList = [].slice.call(document.querySelectorAll('.toast'))
        var toastList = toastElList.map(function (toastEl) {
            return new bootstrap.Toast(toastEl);
        })
        toastList.forEach(toast => toast.show())
    }
}